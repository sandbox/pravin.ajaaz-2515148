CONTENTS OF THIS FILE
---------------------

 * About Entity Bulk Delete UI
 * Requirements
 * Installation
 * Configuration

ABOUT ENTITY BULK DELETE UI
------------

Entity Bulk Delete UI module module will provide an interface to bulk delete 
all drupal entities.


REQUIREMENTS
------------

This module requires entity module.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

This module provides an UI to bulk delete drupal entities.
